
const container = document.querySelector('.container');
const shortenForm = document.getElementById('shorten-form');
const shortenInput = document.getElementById('shorten-input');
const shortenButton = document.getElementById('shorten-button');
const shortenResult = document.getElementById('shorten-result');
const shortenedUrl = document.getElementById('shortened-url');


shortenForm.addEventListener('submit', (event) => {
    event.preventDefault();

    const url = shortenInput.value.trim();

    if (url !== '') {
        const data = {
            url: url
        }

        fetch('https://link.pinapelz.com/api/new_shorten', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => {
            if(!response.ok) {
                shortenedUrl.textContent = "Failed to shorten URL";
                shortenResult.style.display = 'block';
                container.classList.add('show-result');
                throw Error(response.statusText);
            }
            return response.text();
        })
        .then(result => {
            console.log('Success:', result);
            const shortened = result; 
            shortenedUrl.href = shortened;
            shortenedUrl.textContent = shortened;
            shortenResult.style.display = 'block';
            container.classList.add('show-result');
        })
        .catch(error => {
            console.error('Error:', error);
        });
        shortenInput.value = '';
    }
});


